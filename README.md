# Build Push to Humanitec Job

Gitlab CI Job for easy importing of Gitlab based projects into Humanitec.

## Requirements:

Set `HUMANITEC_TOKEN` and `HUMANITEC_ORG` in your group's or project's CI/CD pipelines settings.

## Use:

In the .gitlab-ci.yml file of your project:

```yaml
stages:
  - get-humanitec-registry-credentials
  - build-push-to-humanitec
  - notify-new-build-to-humanitec

include:
  - remote: 'https://gitlab.com/rougeot-antoine/build-push-to-humanitec/-/raw/master/build-push-to-humanitec-template.yml'


get-humanitec-registry-credentials:
  extends: .get-humanitec-registry-credentials-template

build-push-to-humanitec:
  extends: .build-push-to-humanitec-template
  variables:
    IMAGE_NAME: your-image
    IMAGE_TAG: latest
  script:
    - # Do whatever you need to build the image: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$IMAGE_NAME:$IMAGE_TAG

notify-new-build-to-humanitec:
  extends: .notify-new-build-to-humanitec-template
  variables:
    IMAGE_NAME: your-image
    IMAGE_TAG: latest
```